# GNOME Release Service

The GNOME Release Service API provides an interface for GNOME maintainers to release new versions of specific modules or entire suites.

## How to Use the API with Your Module

This API is designed exclusively for use within GitLab CI pipelines. To interact with the API, you must make calls from a job defined in your `.gitlab-ci.yml` file ([learn more](https://docs.gitlab.com/ee/ci/)).

### Requirements for Module Release

To successfully release your module via this API, the following conditions must be met:

- **Tarball Packaging**: The release must be packaged as a tarball.
- **Tarball Naming**: The tarball name must follow the format `<module-name>-<version>.tar.<gz|xz|bz2>`. The `<module-name>` should match your module's name in GitLab (`gitlab.gnome.org/GNOME/<module-name>`).
- **Uploading the Tarball**: The tarball must be uploaded from a project in group [GNOME](https://gitlab.gnome.org/GNOME) via GitLab CI to the API endpoint `/install-module-release`. This is mandatory due to authentication constraints.

### Simplified Integration Using Predefined Templates

For a seamless setup, we recommend using the predefined job template `release-module` provided in the [release-module.yml](ci-templates/release-module.yml) file. This template handles most of the complexity for you.

To use this template, simply provide the tarball artifact path via the environment variable `TARBALL_ARTIFACT_PATH`.

#### Example Configuration in `.gitlab-ci.yml`

```yaml
include:
  - project: 'Infrastructure/openshift-images/gnome-release-service'
    file: '/ci-templates/release-module.yml'

variables:
  TARBALL_ARTIFACT_PATH: <path-to-release-tarball> # <-- Adjust this to point to your tarball's artifact path

release-module:
  extends: .release-module
```

### Implementing Custom API Integration

If you prefer, you can create a custom implementation to interact with the API. However, **this interaction must still be executed from within a GitLab CI job** to comply with authentication requirements. Direct access to the API outside of GitLab CI pipelines is strictly prohibited.

For guidance on structuring your integration, refer to the `release-module` job template provided in [release-module.yml](ci-templates/release-module.yml).

## How to Run Locally

1. **Prerequisites:**
   - Python version 3.12.
   - Install the required dependencies:

     ```bash
     pip install -r requirements.txt
     ```

2. **Set Up Environment Variables** (you can use a `.env` file for convenience):
   - **`JWKS_URI`** *(string)*: The URI of the server that provides JWKS for JWT token validation. Usually set to:  
     `"https://gitlab.gnome.org/oauth/discovery/keys"`.
   - *(Optional)* **`DEBUG`** *(bool)*: Enables debugging mode for local development.

     Example `.env` file:

     ```bash
     DEBUG=True
     JWKS_URI="https://gitlab.gnome.org/oauth/discovery/keys"
     ```

3. **Start the API:**

   Run the following command to start the API:

   ```bash
   python main.py
   ```

   The API will run on host = "0.0.0.0" and port = 8000.

## Testing

The project includes both end-to-end tests and unit tests:

End-to-End Tests: Validate the API's functionality, ensuring correct status codes in responses.
Unit Tests: Test the core functionality of the ModulesManager class, which handles installation requests.
To run the tests, use:

```bash
pytest tests/
```

> Note: If you have a `.env` file created, it may affect the test behavior. It is recommended to either delete or comment out the `.env` file contents before running the tests.

### Manual Testing Tools

If needed, the project provides tools for manual testing, located in the [testing tools](./tests/testing_tools/) directory:

- **`manual_client.py`**  
  A simple client for manual API testing.  
  > Note: This is not an official client and is only intended for basic testing.  

  Use `--help` for usage instructions:

  ```bash
  python manual_client.py --help
  ```

  Example usage (assuming the API uses `jwks_provider.py` for JWT validation and you have a tarball in the current directory, e.g., gnome-calculator-40.0.tar.xz):

  ```bash
  python manual_client.py module gnome-calculator-40.0.tar.xz
  ```

- **`jwks_provider.py`**  
  A mock server for validating JWT tokens, designed for use with `manual_client.py`.  
  Start the server with:

  ```bash
  python jwks_provider.py
  ```

  The server will run on <http://0.0.0.0:8086>. Use this URL for JWKS_URI during testing.
