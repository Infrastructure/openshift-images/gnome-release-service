import jwt
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization

# !!! THESE KEYS WERE GENERATED USING FUNCTION IN THIS SCRIPT AND SERVE NO PURPOSE OTHER THAN DEBUGGING !!!
private_key_str = """-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAn34616e+aVwZm5dnXi36RzD6PvgThKCBWXERhWZqHQ+K9NY3
eg3GH9Ue+GtZBxithwLTKruVhpv9x5PRpPvzF2sfJ8+cieZqj4Gf9//4HQLg2/6l
w+8lzEVpbgMU4xYY/hWS1PpZH/TLYbryib/ns2JQddVkcOPKHpv/DVX98xQnBtHI
F35WZxUFXpVcymowxQflGQNNOaJsxiFPRvkB0SDuASPo0p83JPRvD9utUCvVfL6B
So5HVJMKCA247RGOSxpwqmgy3Xds6ceM4m9Dzk6514TCrtyqnJfRZTvGDVu4ax03
kwcFnyc/ZSe7jEN881oZR8RDkCH6eBNiVDMDawIDAQABAoIBABobV/dn6kelvz1U
NaPZY8/WaQf4pH0/ppyk4OjUoEb8fdLg47jTW1xnjse8DpkWRYVFhZpHlUyw6QQL
orPeV+a77qAeIDIEaOnvnmdCW3MoPvVTw/+D/ExpCj5rO7aGLfYdQukgx4jvEYGA
T+YTZxJaY/A9y1CdQL7s6v1cV5K7tbbEIsNUS2GshlXY877nDx9DKbUihxL3XSq6
ef+Y3eG2MoXcaV8O01AcNekqou0BEEK9D18YkzBH9AGBdQ31Kn6L3a5TUs1KqrBz
PVU2fju24IrxZMUgqBV3bHk9KYdAVjeO/oU7wNMW+XVVZ9Np2hIZARoDyC0CBvTc
aC5h0hkCgYEA4MXRzwRo+EtUV879pn90uRldM9z7dI73+C7CTJjNlVqn6zGyd6MS
bvgVxZYM24eCF+g8RWK+vhvUvY0VMo5aRgXMWYUaWI3Yz/5epIzaZSKfWCjR6tUG
EAvS3i+zXMTovJ3+o8vwkygY/VcT+1cf0JuBufijqlmdafzeYzat0vMCgYEAtaaz
EiHp29L+bPPTehyMWKz15m6UqYkdW2B7EapFZmlSYbQoBPCXnoW5+IQXlAtpH2o2
2Ga4XotkgQSI/tlSyuWK4+S3Y0l8wCPQVsDzCb1y+dS9OxalMELFHlbX95XkBxRW
EiS/OSLC4fvG24YtSm5GhffZD9ht91NPUUKIe6kCgYAgdzI4zpBC3M6P6Evm0p4P
ZkR6Cz8etM/IPyi9W4zGN9PtOr5mXV16PccXRPcSdl4qCdc6xmp6lxaBUnFvXfRY
DNpZS8pkZAie8kTFS7r3xKpzV2IKoz4+7xZEFWu4yJA9Pizbh8d12eLSJNO0B/+G
aA+yxuEM5+i+uFF+zxUIVQKBgBllBp/AUftDUgJVS/lESHKVK4NdbjcNTidaI5pi
nXUlwDicgQO/xtKIi2ksvkcaGUYlCSnTom7FOfW3dpd6NlLbKEGX1fNxcJlD6HpY
WyG/dHVswk+iaNGIZBpLtcGNGOXMeAPtUzZfn7MMCc7iwS4ooH9WGOhYL3dmFC6e
CIqRAoGAIEJMqxa8NS38mCT6cEmI9xugjQH7XreLrK8XqY5iZdMCBNwFfVnEbXw2
+5K53Z+aCKp5aHjt/vNM0mN7t6hPWa8yyuNBdCEiJ48k6Z0Wh/bjpDXnvnOHgpRm
6eN/H457xwBkQs1ztyYxKprQ8RxoYRQ5I2MmSYFxtHkYGImseJE=
-----END RSA PRIVATE KEY-----"""
public_key_str = """-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAn34616e+aVwZm5dnXi36
RzD6PvgThKCBWXERhWZqHQ+K9NY3eg3GH9Ue+GtZBxithwLTKruVhpv9x5PRpPvz
F2sfJ8+cieZqj4Gf9//4HQLg2/6lw+8lzEVpbgMU4xYY/hWS1PpZH/TLYbryib/n
s2JQddVkcOPKHpv/DVX98xQnBtHIF35WZxUFXpVcymowxQflGQNNOaJsxiFPRvkB
0SDuASPo0p83JPRvD9utUCvVfL6BSo5HVJMKCA247RGOSxpwqmgy3Xds6ceM4m9D
zk6514TCrtyqnJfRZTvGDVu4ax03kwcFnyc/ZSe7jEN881oZR8RDkCH6eBNiVDMD
awIDAQAB
-----END PUBLIC KEY-----"""

private_key = serialization.load_pem_private_key(
    private_key_str.encode(), password=None, backend=default_backend()
)

public_key = serialization.load_pem_public_key(
    public_key_str.encode(), backend=default_backend()
)


def generate_keys():
    """
    Generate RSA private and public keys.

    Returns:
    - Tuple containing private and public keys as strings.
    """
    # Generate RSA key pair
    private_key = rsa.generate_private_key(
        public_exponent=65537, key_size=2048, backend=default_backend()
    )
    public_key = private_key.public_key()

    # Serialize private key
    private_pem = private_key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption(),
    )

    # Serialize public key
    public_pem = public_key.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo,
    )

    return private_pem.decode(), public_pem.decode()


def sign_payload(payload):
    """
    Sign a payload using the provided private key.

    Args:
    - payload: Dictionary containing the payload data.

    Returns:
    - JWT token as a string.
    """
    headers = {"kid": "debug-key", "alg": "RS256"}
    return jwt.encode(payload, private_key, algorithm="RS256", headers=headers)


def get_jwks():
    """
    Generate JSON Web Key Set (JWKS) from the provided public key.

    Returns:
    - Dictionary containing public JWK.
    """
    public_jwk = {
        "kty": "RSA",
        "kid": "debug-key",
        "use": "sig",
        "alg": "RS256",
        "n": jwt.utils.to_base64url_uint(public_key.public_numbers().n),
        "e": jwt.utils.to_base64url_uint(public_key.public_numbers().e),
    }
    return {"keys": [public_jwk]}
