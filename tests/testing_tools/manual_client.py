import requests
from pprint import pprint
import argparse
import sys
import os

from debug_jwt import sign_payload
from utils import extract_module_release_info_from_tarball_name

url = "http://localhost:8000/"


def send_tarball(filepath, jwt=None):
    install_url = url + "install-module-release"
    with open(filepath, "rb") as file:
        files = {"tarball": file}
        if jwt:
            with open(jwt, "r") as file:
                token = file.read()
        else:
            module, _, _, _, _ = extract_module_release_info_from_tarball_name(
                os.path.basename(filepath)
            )
            token = sign_payload(
                {
                    "name": "John Doe",
                    "iat": 1516239022,
                    "namespace_path": "GNOME",
                    "ref_type": "tag",
                    "ref_protected": "true",
                    "project_path": f"GNOME/{module}",
                }
            )

        headers = {"Authorization": f"Bearer {token}"}
        res = requests.post(install_url, files=files, headers=headers)
        return res


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers(dest="command")

    module_parser = subparsers.add_parser("module")
    module_parser.add_argument(
        "-j", "--jwt", required=False, default=None, help="Path to JWT token."
    )
    module_parser.add_argument("tarball", help="Path of the tarball to install.")

    args = parser.parse_args()

    if args.command is None:
        parser.print_help()
        sys.exit(1)

    if args.command == "module":
        response = send_tarball(args.tarball, args.jwt)

    print(response.status_code)
    pprint(response.json())
