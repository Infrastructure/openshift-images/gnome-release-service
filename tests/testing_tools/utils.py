import re


def extract_module_release_info_from_tarball_name(
    tarball_name: str,
) -> tuple[str, str, str, str | None]:
    """
    Parse a tarball name to retrieve info about the module release, such as module name, version, etc.

    Args:
        tarball_name (str): The tarball file name.

    Returns:
        tuple[str, str, str, str | None]: A tuple containing the module name, separator, version, file extension, and compression method

    Raises:
        ValueError: If the tarball name is not in the correct format.
    """
    re_module_release = re.compile(
        r"^(?P<module>.*?)"  # Module
        r"(?P<separator>[_-])"  # Separator
        r"(?:(?P<oldversion>[0-9]+\.)+[a-z0-9]+-)?"  # Optional old version
        r"(?P<version>(?:[0-9]+\.)*"  # Version number
        r"(?:alpha|beta|rc)?[0-9\.]*)"  # Optional alpha, beta, or rc suffix
        r"\.(?P<file_extension>tar)"  # tar extension
        r"(?:\.(?P<compress_method>[a-z][a-z0-9]*))?"  # Optional compression method
        r"$"  # End of string
    )
    match = re_module_release.match(tarball_name)
    if match:
        module: str = match.group("module")
        seperator: str = match.group("separator")
        version: str = match.group("version")
        file_extension: str = match.group("file_extension")
        compress_method: str | None = match.group("compress_method")
        return (module, seperator, version, file_extension, compress_method)
    else:
        raise ValueError(f"Invalid tarball name: {tarball_name}")
