from fastapi import FastAPI

from debug_jwt import get_jwks

app = FastAPI()


@app.get("/")
def jwks():
    return get_jwks()


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=8086)
