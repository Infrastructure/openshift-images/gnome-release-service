import pytest
import subprocess
import time
import shutil
import os


@pytest.fixture(scope="function")
def jwks_provider_server():
    """Starts the JWKS provider server in a subprocess for each test."""
    server_process = subprocess.Popen(
        ["python", "jwks_provider.py"],
        cwd=os.path.join(
            os.path.dirname(__file__), "../testing_tools"
        ),  # Path to the jwks_provider.py file
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    time.sleep(2)
    yield
    server_process.terminate()
    server_process.wait()


@pytest.fixture(scope="function")
def fastapi_server():
    """Starts the FastAPI server in a subprocess for each test."""
    server_process = subprocess.Popen(
        ["python", "main.py"],
        cwd=os.path.join(
            os.path.dirname(__file__), "../../"
        ),  # Path to the main.py file
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    time.sleep(2)  # Give the server time to start
    yield
    server_process.terminate()
    server_process.wait()
    # remove the the directory created by the server
    shutil.rmtree(
        os.path.join(os.path.dirname(__file__), "../../sources"), ignore_errors=True
    )


@pytest.fixture(scope="function")
def api_url():
    """Provides the base URL for the API."""
    yield "http://0.0.0.0:8000"
