from contextlib import contextmanager
import os
import requests
import tempfile
import time

from tests.testing_tools.debug_jwt import sign_payload
from tests.testing_tools.utils import extract_module_release_info_from_tarball_name


@contextmanager
def payload(tarball_path):
    """
    Create the payload for the request.

    Args:
        tarball_path (str): The path to the tarball file.

    Returns:
        tuple: A tuple containing the files and headers for the request.
    """
    print("tarball_path")
    print(tarball_path)
    with open(tarball_path, "rb") as file:
        files = {"tarball": file}
        module, *_ = extract_module_release_info_from_tarball_name(
            os.path.basename(tarball_path)
        )
        headers = {
            "Authorization": "Bearer "
            + sign_payload(
                {
                    "name": "John Doe",
                    "iat": int(time.time()),  # Current time
                    "exp": int(time.time()) + 3600,  # Expires after one hour
                    "aud": "gnome-release-service",
                    "namespace_path": "GNOME",
                    "ref_type": "tag",
                    "ref_protected": "true",
                    "project_path": "GNOME/" + module,
                }
            )
        }
        yield files, headers


class TestInstallModuleRelease:
    """
    Test cases for installing a module release.
    This class tests the endpoint /install-module-release.
    """

    endpoint = "/install-module-release"

    def test_no_auth_401(
        self,
        set_env_jwks_uri_local,
        jwks_provider_server,
        fastapi_server,
        api_url,
        dummy_module_40_0_tarball_path,
    ):
        """
        Test that a 401 error is returned when no authentication is provided.
        """
        url = api_url + self.endpoint
        with payload(dummy_module_40_0_tarball_path) as (files, headers):
            headers.pop("Authorization")
            response = requests.post(url, files=files, headers=headers)
            assert response.status_code == 401, response.text

    def test_incorrect_tarball_name_400(
        self,
        set_env_jwks_uri_local,
        jwks_provider_server,
        fastapi_server,
        api_url,
        dummy_module_40_0_tarball_path,
    ):
        """
        Test that a 400 error is returned when the tarball name is incorrect.
        """
        url = api_url + self.endpoint
        with payload(dummy_module_40_0_tarball_path) as (files, headers):
            with tempfile.TemporaryDirectory() as tempdir:
                file_path = os.path.join(tempdir, "incorrect.tar.xz")
                with open(file_path, "wb") as file:
                    file.write(b"stuff")
                with open(file_path, "rb") as file:
                    files["tarball"] = file
                    response = requests.post(url, files=files, headers=headers)
                    assert response.status_code == 400, response.text

    def test_no_tarball_given_422(
        self,
        set_env_jwks_uri_local,
        jwks_provider_server,
        fastapi_server,
        api_url,
        dummy_module_40_0_tarball_path,
    ):
        """
        Test that a 422 error is returned when no tarball is given.
        """
        url = api_url + self.endpoint
        with payload(dummy_module_40_0_tarball_path) as (_, headers):
            response = requests.post(url, headers=headers)
            assert response.status_code == 422, response.text

    def test_missing_claims_403(
        self,
        set_env_jwks_uri_local,
        jwks_provider_server,
        fastapi_server,
        api_url,
        dummy_module_40_0_tarball_path,
    ):
        """
        Test that a 403 error is returned when the required claims are missing.
        """
        url = api_url + self.endpoint
        with payload(dummy_module_40_0_tarball_path) as (files, headers):
            headers["Authorization"] = "Bearer " + sign_payload(
                {
                    "name": "John Doe",
                    "iat": int(time.time()),  # Current time
                    "exp": int(time.time()) + 3600,  # Expires after one hour
                    "aud": "gnome-release-service",
                    # claims missing ...
                }
            )
            response = requests.post(url, files=files, headers=headers)
            assert response.status_code == 403, response.text

    def test_install_tarball_to_foreign_module_403(
        self,
        set_env_jwks_uri_local,
        jwks_provider_server,
        fastapi_server,
        api_url,
        dummy_module_40_0_tarball_path,
    ):
        """
        Test that a 403 error is returned when trying to install a tarball to a foreign module -
        meaning the project_path is not the same as the module name.
        """
        url = api_url + self.endpoint
        with payload(dummy_module_40_0_tarball_path) as (files, headers):
            headers["Authorization"] = "Bearer " + sign_payload(
                {
                    "name": "John Doe",
                    "iat": int(time.time()),  # Current time
                    "exp": int(time.time()) + 3600,  # Expires after one hour
                    "aud": "gnome-release-service",
                    "namespace_path": "GNOME",
                    "ref_type": "tag",
                    "ref_protected": "true",
                    "project_path": "GNOME/"
                    + "different_module",  # different_module != dummy_module
                }
            )
            response = requests.post(url, files=files, headers=headers)
            assert response.status_code == 403, response.text

    def test_install_tarball_from_foreign_namespace_403(
        self,
        set_env_jwks_uri_local,
        jwks_provider_server,
        fastapi_server,
        api_url,
        dummy_module_40_0_tarball_path,
    ):
        """
        Test that a 403 error is returned when trying to install a tarball from a foreign namespace.
        """
        url = api_url + self.endpoint
        with payload(dummy_module_40_0_tarball_path) as (files, headers):
            headers["Authorization"] = "Bearer " + sign_payload(
                {
                    "name": "John Doe",
                    "iat": int(time.time()),  # Current time
                    "exp": int(time.time()) + 3600,  # Expires after one hour
                    "aud": "gnome-release-service",
                    "namespace_path": "foreign_namespace",
                    "ref_type": "tag",
                    "ref_protected": "true",
                    "project_path": "foreign_namespace/" + "dummy-module",
                }
            )
            response = requests.post(url, files=files, headers=headers)
            assert response.status_code == 403, response.text

    def test_successful_installation_gnome_200(
        self,
        set_env_jwks_uri_local,
        jwks_provider_server,
        fastapi_server,
        api_url,
        dummy_module_40_0_tarball_path,
    ):
        """
        Test that a 200 status code is returned when the installation of a module in GNOME group is successful.
        """
        url = api_url + self.endpoint
        with payload(dummy_module_40_0_tarball_path) as (files, headers):
            response = requests.post(url, files=files, headers=headers)
            assert response.status_code == 200, response.text

    def test_successful_installation_incubator_200(
        self,
        set_env_jwks_uri_local,
        jwks_provider_server,
        fastapi_server,
        api_url,
        dummy_module_40_0_tarball_path,
    ):
        """
        Test that a 200 status code is returned when the installation of a module in Incubator group is successful.
        """
        url = api_url + self.endpoint
        with payload(dummy_module_40_0_tarball_path) as (files, headers):
            headers["Authorization"] = "Bearer " + sign_payload(
                {
                    "name": "John Doe",
                    "iat": int(time.time()),  # Current time
                    "exp": int(time.time()) + 3600,  # Expires after one hour
                    "aud": "gnome-release-service",
                    "namespace_path": "GNOME/Incubator",
                    "ref_type": "tag",
                    "ref_protected": "true",
                    "project_path": "GNOME/Incubator/dummy-module",
                }
            )
            response = requests.post(url, files=files, headers=headers)
            assert response.status_code == 200, response

    def test_install_same_module_twice_400(
        self,
        set_env_jwks_uri_local,
        jwks_provider_server,
        fastapi_server,
        api_url,
        dummy_module_40_0_tarball_path,
    ):
        """
        Test that a 400 status code is returned when the same module is installed twice.
        """
        url = api_url + self.endpoint
        with payload(dummy_module_40_0_tarball_path) as (files, headers):
            requests.post(url, files=files, headers=headers)
            response2 = requests.post(url, files=files, headers=headers)
            assert response2.status_code == 400, response2.text

    def test_install_not_triggered_by_tag_403(
        self,
        set_env_jwks_uri_local,
        jwks_provider_server,
        fastapi_server,
        api_url,
        dummy_module_40_0_tarball_path,
    ):
        """
        Test that a 403 status code is returned when the installation is not triggered by a tag.
        """
        url = api_url + self.endpoint
        with payload(dummy_module_40_0_tarball_path) as (files, headers):
            headers["Authorization"] = "Bearer " + sign_payload(
                {
                    "name": "John Doe",
                    "iat": int(time.time()),  # Current time
                    "exp": int(time.time()) + 3600,  # Expires after one hour
                    "aud": "gnome-release-service",
                    "namespace_path": "GNOME",
                    "ref_type": "branch",  # <-- branch instead of tag
                    "ref_protected": "true",
                    "project_path": "GNOME/dummy-module",
                }
            )
            response = requests.post(url, files=files, headers=headers)
            assert response.status_code == 403, response.text
