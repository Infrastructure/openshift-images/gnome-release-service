import pytest
import os


@pytest.fixture(scope="function")
def set_env_debug():
    """Sets the DEBUG environment variable to True."""
    os.environ["DEBUG"] = "True"
    yield
    os.environ.pop("DEBUG", None)


@pytest.fixture(scope="function")
def set_env_jwks_uri_local():
    """Sets the JWKS_URI environment variable to local address."""
    os.environ["JWKS_URI"] = "http://0.0.0.0:8086"
    yield
    os.environ.pop("JWKS_URI", None)


@pytest.fixture(scope="function")
def set_env_jwks_uri_gitlab():
    """Sets the JWKS_URI environment variable to GitLab address."""
    os.environ["JWKS_URI"] = "https://gitlab.gnome.org/oauth/discovery/keys"
    yield
    os.environ.pop("JWKS_URI", None)


@pytest.fixture(scope="function", autouse=True)
def unset_env_vars():
    """
    A session-level fixture that unsets the DEBUG and JWKS_URI environment variables.
    This fixture is automatically used by all tests.
    """
    os.environ.pop("DEBUG", None)
    os.environ.pop("JWKS_URI", None)


@pytest.fixture(scope="function")
def dummy_module_40_0_tarball_path():
    """A dummy module with a version of 40.0."""
    yield os.path.join(
        os.path.dirname(__file__), "test_resources/dummy-module-40.0.tar.gz"
    )


@pytest.fixture(scope="function")
def dummy_module_40_1_tarball_path():
    """A dummy module with a version of 40.1."""
    yield os.path.join(
        os.path.dirname(__file__), "test_resources/dummy-module-40.1.tar.gz"
    )


@pytest.fixture(scope="function")
def other_dummy_module_40_0_tarball_path():
    """Another dummy module with a version of 40.0."""
    yield os.path.join(
        os.path.dirname(__file__), "test_resources/other-dummy-module-40.0.tar.gz"
    )
