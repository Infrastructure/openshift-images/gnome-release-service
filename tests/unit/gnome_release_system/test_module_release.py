import os

from gnome_release_service.gnome_release_system.modules.module_release import (
    ModuleRelease,
    Tarball,
)


class TestModuleReleae:
    """
    Test cases for the ModuleRelease class.
    """

    def test_same_releases_equal(
        self,
        dummy_module_40_0_tarball_path: str,
    ):
        """
        Test case for the __eq__ method of the ModuleRelease class.
        """
        module, seperator, version, _, _ = (
            ModuleRelease.extract_module_release_info_from_tarball_name(
                os.path.basename(dummy_module_40_0_tarball_path)
            )
        )
        tarball = Tarball(dummy_module_40_0_tarball_path)

        module_release_1 = ModuleRelease(
            f"{module}{seperator}{version}", module, version, tarball
        )
        module_release_2 = ModuleRelease(
            f"{module}{seperator}{version}", module, version, tarball
        )

        assert (
            module_release_1 == module_release_2
        ), "ModuleReleases with same name not equal"

    def test_different_releases_not_equal(
        self,
        dummy_module_40_0_tarball_path: str,
        dummy_module_40_1_tarball_path: str,
    ):
        """
        Test case for the __eq__ method of the ModuleRelease class.
        """
        module_1, seperator_1, version_1, _, _ = (
            ModuleRelease.extract_module_release_info_from_tarball_name(
                os.path.basename(dummy_module_40_0_tarball_path)
            )
        )
        tarball_1 = Tarball(dummy_module_40_0_tarball_path)

        module_2, seperator_2, version_2, _, _ = (
            ModuleRelease.extract_module_release_info_from_tarball_name(
                os.path.basename(dummy_module_40_1_tarball_path)
            )
        )
        tarball_2 = Tarball(dummy_module_40_1_tarball_path)

        module_release_1 = ModuleRelease(
            f"{module_1}{seperator_1}{version_1}", module_1, version_1, tarball_1
        )
        module_release_2 = ModuleRelease(
            f"{module_2}{seperator_2}{version_2}", module_2, version_2, tarball_2
        )

        assert (
            module_release_1 != module_release_2
        ), "ModuleReleases with different names equal"

    def test_extract_special_files(
        self,
        dummy_module_40_0_tarball_path: str,
    ):
        """
        Test case for the _extract_special_files method of the ModuleRelease class.

        The prerequisites for this test are:
        - The tarball has a NEWS file.
        """
        module, seperator, version, _, _ = (
            ModuleRelease.extract_module_release_info_from_tarball_name(
                os.path.basename(dummy_module_40_0_tarball_path)
            )
        )
        tarball = Tarball(dummy_module_40_0_tarball_path)

        module_release = ModuleRelease(
            f"{module}{seperator}{version}", module, version, tarball
        )

        assert "news" in module_release.special_files, "NEWS file not extracted"
        assert (
            len(module_release.special_files) == 1
        ), "Expected only NEWS file to be extracted"

    def test_extract_module_release_info_from_tarball_name_returns_module_seperator_version_fileext_compressmethod(
        self,
    ):
        """
        Test case for the extract_module_release_info_from_tarball_name method of the ModuleRelease class.
        """
        module_release_name = "dummy-module-40.0.tar.xz"

        module, seperator, version, fileext, compressmethod = (
            ModuleRelease.extract_module_release_info_from_tarball_name(
                module_release_name
            )
        )

        assert module == "dummy-module", "Module name not extracted"
        assert seperator == "-", "Seperator not extracted"
        assert version == "40.0", "Version not extracted"
        assert fileext == "tar", "File extension not extracted"
        assert compressmethod == "xz", "Compression method not extracted"
