import pytest
import os
import tempfile
import tarfile

from gnome_release_service.gnome_release_system.tarball import Tarball
from gnome_release_service.gnome_release_system.exceptions import (
    FileNotTarballException,
)


@pytest.fixture(scope="function")
def tmp_dir():
    """
    Fixture for a temporary path.
    """
    with tempfile.TemporaryDirectory() as tmp_dir:
        yield tmp_dir


class TestTarball:
    """
    Unit tests for the Tarball class.
    """

    def test_tarball_not_tarball_exception(self, tmp_dir):
        """
        Test case for the __init__ method of the Tarball class when the file is not a tarball.
        """
        dummy_file_path = os.path.join(tmp_dir, "dummy_file.txt")
        with open(dummy_file_path, "w") as f:
            f.write("dummy file")

        with pytest.raises(FileNotTarballException):
            Tarball(dummy_file_path)

    def test_extract_all_files(self, dummy_module_40_0_tarball_path: str, tmp_dir):
        """
        Test case for the extract method of the Tarball class when all files are extracted.
        """
        tarball = Tarball(dummy_module_40_0_tarball_path)
        tarball.extract(tmp_dir)

        with tempfile.TemporaryDirectory() as other_tmp_dir:
            with tarfile.open(dummy_module_40_0_tarball_path, "r") as tar:
                tar.extractall(other_tmp_dir, filter=tarfile.data_filter)

            assert len(os.listdir(tmp_dir)) == len(
                os.listdir(other_tmp_dir)
            ), "Not all files extracted"

    def test_extract_desired_files(self, dummy_module_40_0_tarball_path: str, tmp_dir):
        """
        Test case for the extract method of the Tarball class when desired files are extracted.
        """
        tarball = Tarball(dummy_module_40_0_tarball_path)
        desired_file = "dummy-module-40.0/NEWS"
        tarball.extract(tmp_dir, [desired_file])
        assert os.path.exists(
            os.path.join(tmp_dir, desired_file)
        ), "Desired file not extracted"

    def test_list_files(self, dummy_module_40_0_tarball_path: str):
        """
        Test case for the list_files method of the Tarball class.
        """
        tarball = Tarball(dummy_module_40_0_tarball_path)
        files = tarball.list_files()

        with tarfile.open(dummy_module_40_0_tarball_path, "r") as tar:
            expected_files = tar.getnames()

        assert files == expected_files, "Files not correct"

    def test_get_compression_gz(self, dummy_module_40_0_tarball_path: str):
        """
        Test case for the get_compression method of the Tarball class.
        """

        assert dummy_module_40_0_tarball_path.endswith(".gz"), "Test file not correct"

        tarball = Tarball(dummy_module_40_0_tarball_path)
        compression = tarball.get_compression()

        assert compression == "gz", "Compression not correct"
