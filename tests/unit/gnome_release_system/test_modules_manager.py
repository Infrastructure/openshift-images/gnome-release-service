import pytest
import os
import shutil

from gnome_release_service.gnome_release_system import ModuleRelease
from gnome_release_service.gnome_release_system import Utils
from gnome_release_service.gnome_release_system import Tarball
from gnome_release_service.gnome_release_system.modules_manager import ModulesManager
from gnome_release_service.gnome_release_system.exceptions import (
    ModuleVersionAlreadyInstalledException,
)

from tests.testing_tools.utils import extract_module_release_info_from_tarball_name


@pytest.fixture(scope="function")
def modules_manager():
    """
    Fixture for the ModulesManager class.
    """
    modules_manager = ModulesManager()
    yield modules_manager
    shutil.rmtree(modules_manager.modules_dir, ignore_errors=True)


class TestModulesManager:
    """
    Test cases for the ModulesManager class.
    """

    def test_init_directories_created(
        self, modules_manager: ModulesManager, set_env_debug
    ):
        """
        Test case for the __init__ method of the ModulesManager class.
        """
        assert os.path.exists(modules_manager.modules_dir), "modules_dir not created"

    def test_install_module_release(
        self,
        modules_manager: ModulesManager,
        dummy_module_40_0_tarball_path: str,
        set_env_debug,
    ):
        """
        Test case for the install_module_release method of the ModulesManager class.
        """
        file_name = os.path.basename(dummy_module_40_0_tarball_path)

        module, seperator, version, _, _ = (
            extract_module_release_info_from_tarball_name(file_name)
        )
        tarball = Tarball(dummy_module_40_0_tarball_path)

        module_release = ModuleRelease(
            f"{module}{seperator}{version}", module, version, tarball
        )

        modules_manager.install_module_release(module_release)

        expected_module_release_dir = os.path.join(
            modules_manager.modules_dir,
            module_release.module,
            Utils.get_majmin(module_release.version),
        )
        expected_cache_file_path = os.path.join(
            os.path.dirname(expected_module_release_dir), "cache.json"
        )
        assert os.path.exists(expected_cache_file_path), "cache.json file not found"
        expected_release_tarball_path = os.path.join(
            expected_module_release_dir, file_name
        )
        assert os.path.exists(
            expected_release_tarball_path
        ), f"{file_name} not found in {expected_module_release_dir}"
        expected_checksum_file_path = os.path.join(
            expected_module_release_dir, module_release.name + ".sha256sum"
        )
        assert os.path.exists(
            os.path.join(expected_checksum_file_path)
        ), f"{module_release.name}.sha256sum not found in {expected_module_release_dir}"
        expected_news_file_path = (
            os.path.join(  # under the assumption that the NEWS file is in the tarball
                expected_module_release_dir, module_release.name + ".news"
            )
        )
        assert os.path.exists(
            expected_news_file_path
        ), f"{module_release.name}.news not found in {expected_module_release_dir}"
        expected_symlink_path = os.path.join(
            expected_module_release_dir, f"LATEST-IS-{module_release.version}"
        )
        assert os.path.exists(
            expected_symlink_path
        ), f"LATEST-IS-{module_release.version} not found in {expected_module_release_dir}"

        assert os.path.islink(
            expected_symlink_path
        ), f"{expected_symlink_path} is not a symlink"
        assert os.readlink(expected_symlink_path) == os.path.relpath(
            expected_release_tarball_path, expected_module_release_dir
        ), f"{expected_symlink_path} does not point to {expected_release_tarball_path}"

    def test_same_module_release_cant_be_installed_twice(
        self,
        modules_manager: ModulesManager,
        dummy_module_40_0_tarball_path: str,
        set_env_debug,
    ):
        """
        Test case for the install_module_release method of the ModulesManager class.
        """
        file_name = os.path.basename(dummy_module_40_0_tarball_path)

        module, seperator, version, _, _ = (
            extract_module_release_info_from_tarball_name(file_name)
        )
        tarball = Tarball(dummy_module_40_0_tarball_path)

        module_release = ModuleRelease(
            f"{module}{seperator}{version}", module, version, tarball
        )

        modules_manager.install_module_release(module_release)

        with pytest.raises(ModuleVersionAlreadyInstalledException) as e:
            modules_manager.install_module_release(module_release)

    def test_setup_new_module_info(
        self, modules_manager: ModulesManager, set_env_debug
    ):
        """
        Test case for the setup_new_module_info method of the ModulesManager class.
        """
        module_name = "dummy-module"
        module_info = modules_manager.setup_new_module_info(module_name)

        assert os.path.exists(
            os.path.join(modules_manager.modules_dir, module_name)
        ), "Module directory not created"
        assert module_info.module == module_name, "Module name not set correctly"
        assert module_info.module_dir == os.path.join(
            modules_manager.modules_dir, module_name
        ), "Module directory not set correctly"

    def test_get_module_info(self, modules_manager: ModulesManager, set_env_debug):
        """
        Test case for the get_module_info method of the ModulesManager class.
        """
        module_name = "dummy-module"
        original = modules_manager.setup_new_module_info(module_name)

        retrieved = modules_manager.get_module_info(module_name)

        assert retrieved.module == original.module, "Module name not set correctly"
        assert (
            retrieved.module_dir == original.module_dir
        ), "Module directory not set correctly"
