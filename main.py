import os
import sys
import shutil

from dotenv import load_dotenv

load_dotenv()


import coloredlogs
import logging

coloredlogs.install(
    # level=logging.INFO if not DEBUG else logging.DEBUG,
    level=logging.DEBUG,
    fmt="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
)


from fastapi import FastAPI, UploadFile, File, HTTPException, Form, Request, Depends
from fastapi_jwk_auth.jwks_auth import JWKMiddleware

import tempfile

from gnome_release_service.gnome_release_system import (
    ModuleRelease,
    Tarball,
    ModulesManager,
    ExternalException,
    InternalException,
)

logger = logging.getLogger("gnome_release_service_api")


# Create a FastAPI app
app = FastAPI(
    title="GNOME Release Service API",
)


def add_jwk_middleware(app: FastAPI):
    JWKS_URI = os.getenv("JWKS_URI")
    if not JWKS_URI:
        logger.error("JWKS_URI environment variable is not set")
        sys.exit(1)
    options = {"verify_aud": False}  # Disable audience verification
    app.add_middleware(JWKMiddleware, jwks_uri=os.getenv("JWKS_URI"), options=options)


add_jwk_middleware(app)


def validate_claims(recieved_jwt_payload: dict[str,], required_claims: dict[str,]):
    """
    Validate the claims in the JWT payload.

    Args::
        recieved_jwt_payload (dict[str,]): The payload of the recieved JWT token. The keys are the claim names and the values are the claim values.
        required_claims (dict[str,]): The required claims. The keys are the claim names and the values are the claim values. If the value is a list, the claim value must be one of the values in the list.

    Raises:
        HTTPException: If any of the required claims are missing or have invalid values.
    """
    errs = []
    for claim in required_claims:
        if claim not in recieved_jwt_payload:
            errs.append(f"Required claim {claim} is missing.")
            continue

        if isinstance(required_claims[claim], list):
            if recieved_jwt_payload[claim] not in required_claims[claim]:
                errs.append(f"Required claim {claim} has an invalid value.")
        else:
            if recieved_jwt_payload[claim] != required_claims[claim]:
                errs.append(f"Required claim {claim} has an invalid value.")
    if errs:
        err_msg = " ".join(errs)
        logger.info(
            "The following errors were found during validating claims: %s",
            err_msg,
        )
        raise HTTPException(status_code=403, detail=err_msg)


@app.post("/install-module-release")
async def install_module_release(
    request: Request,  # passed into the dependency
    tarball: UploadFile = File(
        ...,
        description="The tarball to install. The name of the tarball determines the module and the version of the release.",
    ),
):
    """
    Install module release in a tarball to the FTP server.
    """
    try:
        logger.info("Installing module release from tarball: %s", tarball.filename)
        logger.debug("JWT payload: %s", request.state.payload)

        with tempfile.TemporaryDirectory() as temp_dir:
            tarball_path = os.path.join(temp_dir, tarball.filename)
            with open(tarball_path, "wb") as f:
                shutil.copyfileobj(tarball.file, f)
            release_tarball = Tarball(tarball_path)
            module, seperator, version, _, _ = (
                ModuleRelease.extract_module_release_info_from_tarball_name(
                    os.path.basename(release_tarball.tarball_path)
                )
            )
            release_name = f"{module}{seperator}{version}"
            module_release = ModuleRelease(
                release_name, module, version, release_tarball
            )

            # Map of exceptions to pass auth when the module in tarball name does not match the Gitlab project name
            dgo_to_gitlab_map = {
                "gnome-console": "console",
            }
            supported_project_paths = [
                # projects from GNOME itself and GNOME/Incubator
                f"GNOME/{dgo_to_gitlab_map.get(module, module)}",
                f"GNOME/Incubator/{module}",
                # exceptions allowed for specific projects outside GNOME group
                "chergert/ptyxis",
            ]
            required_claims = {
                "ref_protected": "true",
                "ref_type": "tag",
                "project_path": supported_project_paths,
            }
            validate_claims(request.state.payload, required_claims)
            logger.info(
                "JWT claims validated successfully for module release %s",
                module_release.name,
            )

            logger.info("Installing module release %s", module_release.name)
            modules_manager = ModulesManager()
            modules_manager.install_module_release(module_release)
        logger.info("Module release %s installed successfully", module_release.name)
        return {"message": "Module release installed successfully"}

    except ExternalException as e:
        logger.debug("Client error during installing module release: %s", e)
        raise HTTPException(status_code=400, detail=str(e)) from e
    except InternalException as e:
        logger.error("Internal error during installing module release: %s", e)
        raise HTTPException(
            status_code=500, detail="An internal error occurred during installation."
        ) from e
    except HTTPException as e:  # just to log the error
        logger.error("Client error during installing module release: %s", e)
        raise e
    except Exception as e:
        logger.error("Unexpected internal error: %s", e)
        raise HTTPException(
            status_code=500, detail="An unexpected internal error occurred."
        ) from e


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=8000, log_config=None)
