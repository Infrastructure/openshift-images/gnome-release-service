FROM python:3.12-alpine

# Set the working directory to the dir with source code and where the app will run
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . .

# Change the group ownership to root and set group permissions to match user permissions
# This should allow the app to touch the files as OpenShift will run the container as a random user
RUN chgrp -R 0 /app && chmod -R g=u /app

# Install git (as one of the requirements is from github repo) and install the requirements
RUN apk add --no-cache git
RUN pip install -r /app/requirements.txt

# Set the environment variables
ENV JWKS_URI="https://gitlab.gnome.org/oauth/discovery/keys"
# Delete or set to False for production
ENV DEBUG="False"

# Make port 8000 available to the world outside this container
EXPOSE 8000

# Run run API
CMD ["python", "/app/main.py"]
