from __future__ import annotations
from ..logger import logger
import os
import re
import tempfile

from ..tarball import Tarball
from ..exceptions import TarballNameParsingException


class ModuleRelease:

    def __init__(
        self, release_name: str, module_name: str, version: str, tarball: Tarball
    ) -> None:
        self.name = release_name
        self.module = module_name
        self.version = version
        self.tarball = tarball
        self._tempdir = tempfile.TemporaryDirectory(
            prefix=f"gnome-module_release-{self.name}-"
        )
        self.special_files = self._extract_special_files()

    def __eq__(self, other: ModuleRelease) -> bool:
        if not isinstance(other, ModuleRelease):
            return False
        return (
            self.name == other.name
            and self.module == other.module
            and self.version == other.version
        )

    def _extract_special_files(self) -> dict[str, str]:
        """
        Extract special files in the tarball.

        Returns:
            dict[str, str]: A dictionary containing the special files found in the tarball. The key is the intended extension of the file on the server, and the value is the path to the extracted file.
        """
        expected_special_files = {
            # Filename in tarball: extension on ftp.gnome.org
            "NEWS": "news",
            "ChangeLog": "changes",
        }
        special_files_found_in_tarball = {
            # extension on ftp.gnome.org: name of file
            expected_special_files[os.path.basename(file)]: file
            for file in self.tarball.list_files()
            # file is in expected_special_files
            if os.path.basename(file) in expected_special_files.keys()
            # file is in the module root directory
            and os.path.dirname(file) == f"{self.module}-{self.version}"
        }
        self.tarball.extract(
            self._tempdir.name, special_files_found_in_tarball.values()
        )
        return {
            extension: os.path.join(self._tempdir.name, file)
            for extension, file in special_files_found_in_tarball.items()
        }

    @staticmethod
    def extract_module_release_info_from_tarball_name(
        tarball_name: str,
    ) -> tuple[str, str, str, str | None]:
        """
        Parse a tarball name to retrieve info about the module release, such as module name, version, etc.

        Args:
            tarball_name (str): The tarball file name.

        Returns:
            tuple[str, str, str, str | None]: A tuple containing the module name, separator, version, file extension, and compression method

        Raises:
            TarballNameParsingException: If the tarball name cannot be parsed.
        """
        re_module_release = re.compile(
            r"^(?P<module>.*?)"  # Module
            r"(?P<separator>[_-])"  # Separator
            r"(?:(?P<oldversion>[0-9]+\.)+[a-z0-9]+-)?"  # Optional old version
            r"(?P<version>(?:[0-9]+\.)*"  # Version number
            r"(?:alpha|beta|rc)?[0-9\.]*)"  # Optional alpha, beta, or rc suffix
            r"\.(?P<file_extension>tar)"  # tar extension
            r"(?:\.(?P<compress_method>[a-z][a-z0-9]*))?"  # Optional compression method
            r"$"  # End of string
        )
        match = re_module_release.match(tarball_name)
        if match:
            module: str = match.group("module")
            seperator: str = match.group("separator")
            version: str = match.group("version")
            file_extension: str = match.group("file_extension")
            compress_method: str | None = match.group("compress_method")
            return (module, seperator, version, file_extension, compress_method)
        else:
            raise TarballNameParsingException(tarball_name)
