from ..logger import logger
import gitlab
import xmltodict

from ..exceptions import GitlabGetException


def catch_errors_return_none(func):
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception:
            return None

    return wrapper


class GnomeProjectDoap:
    def __init__(self, project_name: str) -> None:
        self._project_name = project_name
        self._doap = self._get_doap()

    @catch_errors_return_none
    def _get_doap(self) -> dict:
        SERVER = "https://gitlab.gnome.org"
        GROUP_NAME = "GNOME"
        gl = gitlab.Gitlab(SERVER, api_version=4)

        # Retrieve the project
        try:
            project = gl.projects.get(f"{GROUP_NAME}/{self._project_name}")
        except gitlab.exceptions.GitlabGetError as e:
            # If the project is not found, return an empty dictionary
            if e.response_code == 404:
                return {}
            logger.error(
                "Failed to retrieve project %s from Gitlab: %s", self._project_name, e
            )
            raise GitlabGetException(self._project_name, SERVER)

        # Look for the DOAP file
        doap_file = None
        for item in project.repository_tree(ref=project.default_branch, iterator=True):
            if item["type"] == "blob" and item["path"].endswith(".doap"):
                doap_file = item
                break

        # If DOAP file is found, retrieve its content
        if doap_file:
            doap_content = project.files.get(
                file_path=doap_file["path"], ref=project.default_branch
            ).decode()
            return xmltodict.parse(doap_content)
        return {}  # Return an empty dictionary if DOAP file is not found

    @catch_errors_return_none
    def refresh(self) -> None:
        self._doap = self._get_doap()

    @catch_errors_return_none
    def get_bug_database(self) -> str | None:
        return (
            self._doap.get("Project", {})
            .get("bug-database", {})
            .get("@rdf:resource", None)
        )

    @catch_errors_return_none
    def get_category(self) -> str | None:
        return (
            self._doap.get("Project", {}).get("category", {}).get("@rdf:resource", None)
        )

    @catch_errors_return_none
    def get_description(self) -> str | None:
        desc = self._doap.get("Project", {}).get("description", {})
        if isinstance(desc, dict):
            return desc.get("#text", None)
        elif isinstance(desc, str):
            return desc
        return None

    @catch_errors_return_none
    def get_download_page(self) -> str | None:
        return (
            self._doap.get("Project", {})
            .get("download-page", {})
            .get("@rdf:resource", None)
        )

    @catch_errors_return_none
    def get_homepage(self) -> str | None:
        return (
            self._doap.get("Project", {}).get("homepage", {}).get("@rdf:resource", None)
        )

    @catch_errors_return_none
    def get_maintainers(self) -> list[dict[str, str | None]]:
        """
        Returns a list of maintainers for the project in a format:
        [
            {
                "email": "maintainer email",
                "name": "maintainer name",
                "userid": "maintainer userid",
            },
            ...
        ]

        If the maintainer does not have a value for a field, it will be None.

        Returns:
            list[dict[str, str | None]]: A list of maintainers for the project.
        """

        @catch_errors_return_none
        def parse_maintainer(maintainer: dict) -> dict[str, str | None]:
            person_node = maintainer.get("foaf:Person", {})
            mbox = person_node.get("foaf:mbox", {})
            if isinstance(mbox, list):
                mail = (
                    mbox[0].get("@rdf:resource", None)
                    if mbox and len(mbox) > 0
                    else None
                )
            else:
                mail = mbox.get("@rdf:resource", None)
            return {
                # Remove "mailto:" from the email
                "email": (mail.split("mailto:")[1] if mail else None),
                "name": person_node.get("foaf:name", None),
                "userid": person_node.get("gnome:userid", None),
            }

        maintainers = []
        maintainer_node = self._doap.get("Project", {}).get("maintainer", {})
        # if there are multiple maintainers, the value is a list of dicts, otherwise it's a single dict
        if isinstance(maintainer_node, list):
            for maintainer in maintainer_node:
                maintainers.append(parse_maintainer(maintainer))
        else:
            maintainers.append(parse_maintainer(maintainer_node))
        return maintainers

    @catch_errors_return_none
    def get_name(self) -> str | None:
        return self._doap.get("Project", {}).get("name", {}).get("#text", None)

    @catch_errors_return_none
    def get_programming_language(self) -> str | None:
        return self._doap.get("Project", {}).get("programming-language", None)

    @catch_errors_return_none
    def get_shortdesc(self) -> str | None:
        return self._doap.get("Project", {}).get("shortdesc", {}).get("#text", None)

    @catch_errors_return_none
    def get_support_forum(self):
        return (
            self._doap.get("Project", {})
            .get("support-forum", {})
            .get("@rdf:resource", None)
        )
