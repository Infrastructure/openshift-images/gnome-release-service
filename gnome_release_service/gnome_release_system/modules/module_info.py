from ..logger import logger
import os
import re
import shutil
import textwrap
import smtplib
from email.mime.text import MIMEText
from email.utils import formataddr, formatdate

from .module_release import ModuleRelease
from .gnome_project_doap import GnomeProjectDoap
from ..utils import Utils
from ..cache import Cache
from ..tarball import Tarball
from ..exceptions import ModuleVersionAlreadyInstalledException


class ModuleInfo:
    """
    Information about a module.

    Attributes:
        module (str): The name of the module.
        module_dir (str): The path to the module directory.
        cache (Cache): The cache object for the module.
    """

    def __init__(self, module: str, module_dir: str) -> None:
        """
        Initializes the ModuleInfo object.
        Creates the module directory if it doesn't exist.

        Args:
            module (str): The name of the module.
            module_dir (str): The path to the module directory.
        """
        self.module = module
        self.module_dir = module_dir
        # Create the module directory if it doesn't exist
        os.makedirs(module_dir, exist_ok=True)
        self._cache = Cache(module_dir)
        self.doap = GnomeProjectDoap(module)

    def get_versions(self) -> list[str]:
        """
        Returns a sorted list of versions for the module.

        Returns:
            list[str]: A sorted list of versions for the module. Last is the latest.
        """
        return self._cache.get_versions(self.module)

    def get_released_version_before(self, version: str) -> str | None:
        """
        Returns the version before the given version.

        Args:
            version (str): The version to get the version before.

        Returns:
            str | None: The version before the given version or None if not found.
        """
        for v in self.get_versions()[::-1]:  # go from latest to oldest
            if Utils.version_cmp(v, version) < 0:  # v < version
                return v
        return None

    def get_files(self, version: str) -> dict[str, str]:
        """
        Returns a list of files for the given version.

        Args:
            version (str): The version to list the files for.

        Returns:
            dict[str, str]: A dictionary of extension: relative file paths (majmin/file.ext).
        """
        return self._cache.get_files(self.module, version)

    def get_latest_version(self) -> str | None:
        """
        Returns the latest version for the module.

        Returns:
            str | None: The latest version for the module or None if not found.
        """
        versions = self.get_versions()
        return versions[-1] if versions else None

    def get_latest_version_of_majmin(self, maj: str) -> str | None:
        """
        Returns the latest version for the given maj.

        Args:
            maj (str): The maj to get the latest version for.

        Returns:
            str | None: The latest version for the given maj or None if not found.
        """
        for version in self.get_versions()[::-1]:  # go from latest to oldest
            if version.startswith(maj):
                return version
        return None

    def get_module_release(self, version: str) -> ModuleRelease | None:
        """
        Returns a ModuleRelease object for the given version.

        Args:
            version (str): The version to get the module_release for.

        Returns:
            ModuleRelease | None: A ModuleRelease object for the given version or None if not found.
        """
        # Get a dict of extension: relative file paths for the given version
        files = self._cache.get_files(self.module, version)
        # Iterate over each file path
        for ext, file_path in files.items():
            # if the file is a tarball, return a ModuleRelease object
            if "tar" in ext:
                path_to_tarball = os.path.join(self.module_dir, file_path)
                module, seperator, version, _, _ = (
                    ModuleRelease.extract_module_release_info_from_tarball_name(
                        os.path.basename(path_to_tarball)
                    )
                )
                release_name = f"{module}{seperator}{version}"
                tarball = Tarball(path_to_tarball)
                return ModuleRelease(release_name, module, version, tarball)
        # Return None if no tarball was found
        return None

    def install_module_release(self, module_release: ModuleRelease) -> None:
        """
        Installs the given module_release.

        Args:
            module_release (ModuleRelease): The module_release to install.

        Raises:
            ModuleVersionAlreadyInstalledException: If the version is already installed.
        """
        logger.info("%s: Installing release", module_release.name)

        if self._is_version_already_installed(module_release):
            raise ModuleVersionAlreadyInstalledException(
                module_release.module, module_release.version
            )

        created_files = []
        try:
            module_release_dir = self._prepare_module_release_directory(module_release)
            released_tarball = self._copy_release_tarball(
                module_release, module_release_dir
            )
            created_files.append(released_tarball)
            special_files_diffed = self._handle_special_files(
                module_release, module_release_dir
            )
            created_files.extend(special_files_diffed.values())
            print(created_files)
            created_files.append(
                self._create_checksum_file(
                    module_release, module_release_dir, created_files
                )
            )
            self._update_cache(module_release)
            self._update_latest_is_symlink(module_release, module_release_dir)
            self._update_cache(module_release)
        except Exception as e:
            logger.exception(
                "%s: Unexpected error during installation: %s", module_release.name, e
            )
            # Remove all created files if an error occurs as the installation failed
            for file_path in created_files:
                os.remove(file_path)
            raise
        self._send_notifications_for_module_release(module_release)
        logger.info("%s: Notifications should be sent.", module_release.name)

    def _update_cache(self, module_release: ModuleRelease):
        """
        Updates the cache for the module.

        Args:
            module_release (ModuleRelease): The module_release to update the cache for.
        """
        self._cache.update_cache()
        logger.info("%s: Cache updated.", module_release.name)

    def _is_version_already_installed(self, module_release: ModuleRelease) -> bool:
        """
        Checks if the given version is already installed.

        Args:
            module_release (ModuleRelease): The module_release to check.

        Returns:
            bool: True if the version is already installed, False otherwise.
        """
        if self.get_module_release(module_release.version):
            logger.info(
                "%s: Version %s of module %s already installed.",
                module_release.name,
                module_release.version,
                module_release.module,
            )
            return True
        return False

    def _prepare_module_release_directory(self, module_release: ModuleRelease) -> str:
        """
        Prepares the directory for the module_release.

        Args:
            module_release (ModuleRelease): The module_release to prepare the directory for.

        Returns:
            str: The path to the directory for the module_release.
        """
        module_release_dir = os.path.join(
            self.module_dir, Utils.get_majmin(module_release.version)
        )
        os.makedirs(module_release_dir, exist_ok=True)
        return module_release_dir

    def _copy_release_tarball(
        self, module_release: ModuleRelease, module_release_dir: str
    ) -> str:
        """
        Copies the release tarball to the module_release directory.

        Args:
            module_release (ModuleRelease): The module_release to copy the tarball for.
            module_release_dir (str): The path to the module_release directory.

        Returns:
            str: The name of the copied tarball.
        """
        tarball_path = os.path.join(
            module_release_dir, os.path.basename(module_release.tarball.tarball_path)
        )
        shutil.copy(module_release.tarball.tarball_path, tarball_path)
        logger.info("%s: Release tarball copied.", module_release.name)
        return tarball_path

    def _handle_special_files(
        self, module_release: ModuleRelease, module_release_dir: str
    ) -> dict[str, str]:
        """
        Handles special files for the module_release.

        Args:
            module_release (ModuleRelease): The module_release to handle the special files for.
            module_release_dir (str): The path to the module_release directory.

        Returns:
            dict[str, str]: A dictionary of special files with the extension as key and the path as value.
        """

        previous_released_version = self.get_released_version_before(
            module_release.version
        )
        previous_module_release = (
            self.get_module_release(previous_released_version)
            if previous_released_version
            else None
        )

        if previous_module_release:
            logger.info(
                "%s: Diffing special files against previous release: %s",
                module_release.name,
                previous_module_release.name,
            )
            return self.diff_special_files(
                module_release.special_files,
                previous_module_release.special_files,
                module_release_dir,
                module_release.name,
            )
        else:
            logger.info(
                "%s: No previous release found. Copying special files as they are.",
                module_release.name,
            )
            return self._copy_special_files_as_is(module_release, module_release_dir)

    def _copy_special_files_as_is(
        self, module_release: ModuleRelease, module_release_dir: str
    ) -> dict[str, str]:
        """
        Copies the special files for the module_release as they are.

        Args:
            module_release (ModuleRelease): The module_release to copy the special files for.
            module_release_dir (str): The path to the module_release directory.

        Returns:
            dict[str, str]: A dictionary of special files with the extension as key and the path as value.
        """
        special_files_diffed = {}
        for ext, new_file_path in module_release.special_files.items():
            special_files_diffed[ext] = os.path.join(
                module_release_dir, f"{module_release.name}.{ext}"
            )
            shutil.copy(new_file_path, special_files_diffed[ext])
        return special_files_diffed

    def _create_checksum_file(
        self,
        module_release: ModuleRelease,
        module_release_dir: str,
        created_files: list[str],
    ) -> str:
        """
        Creates the checksum file for the module_release.
        The checksum file contains the SHA256 hashes of all created files relative to the module_release.

        Args:
            module_release (ModuleRelease): The module_release to create the checksum file for.
            module_release_dir (str): The path to the module_release directory.
            created_files (list[str]): The list of created files.

        Returns:
            str: The name of the checksum file.
        """
        base_name = f"{module_release.name}.sha256sum"
        checksum_file_path = os.path.join(module_release_dir, base_name)
        try:
            with open(checksum_file_path, "w") as f:
                for file_path in created_files:
                    print()
                    print(file_path)
                    print()
                    hash = Utils.calculate_sha256(file_path)
                    f.write(f"{hash}  {os.path.basename(file_path)}\n")
        except Exception:
            os.remove(checksum_file_path)  # remove the checksum file if an error occurs
            raise
        logger.info("%s: Checksum file created.", module_release.name)
        return checksum_file_path

    def _update_latest_is_symlink(
        self,
        module_release: ModuleRelease,
        module_release_dir: str,
    ) -> None:
        """
        Updates the LATEST-IS symlink for the module_release.

        The LATEST-IS symlink points to the latest version of the module in the maj directory.

        Args:
            module_release (ModuleRelease): The module_release to update the LATEST-IS symlink for.
            module_release_dir (str): The path to the module_release directory.
        """
        latest_version_in_module_release_dir = self.get_latest_version_of_majmin(
            os.path.basename(module_release_dir)
        )

        latest_release_of_maj = self.get_module_release(
            latest_version_in_module_release_dir
        )

        self._remove_latest_is_symlinks(module_release_dir)
        latest_is_name = f"LATEST-IS-{latest_release_of_maj.version}"
        latest_is_path = os.path.join(module_release_dir, latest_is_name)
        relative_path_of_symlink_and_latest_release_tarball = os.path.relpath(
            latest_release_of_maj.tarball.tarball_path, os.path.dirname(latest_is_path)
        )
        os.symlink(
            relative_path_of_symlink_and_latest_release_tarball,
            latest_is_path,
        )
        logger.info(
            "%s: LATEST-IS symlink updated to: %s.", module_release.name, latest_is_name
        )

    def _remove_latest_is_symlinks(self, module_release_dir: str) -> None:
        """
        Removes LATEST-IS symlink(s) from the module_release directory.

        Args:
            module_release_dir (str): The path to the module_release directory.
        """
        for file in os.listdir(module_release_dir):
            if file.startswith("LATEST-IS"):
                os.remove(os.path.join(module_release_dir, file))

    def diff_special_files(
        self,
        new_special_files: dict[str, str],
        previous_special_files: dict[str, str],
        dir_with_diff: str,
        module_release_name: str,
    ) -> dict[str, str]:
        """
        Diff special files between two module_releases and save the diff files.

        Args:
            new_special_files (dict[str, str]): The special files for the new module_release.
            previous_special_files (dict[str, str]): The special files for the previous module_release.
            dir_with_diff (str): The directory to save the diff files in.
            module_release_name (str): The name of the module_release.

        Returns:
            dict[str, str]: A dictionary of diff files with the extension as key and the path as value.
        """
        diff_files = {}
        for ext, new_file_path in new_special_files.items():
            previous_file_path = previous_special_files.get(ext)
            diff_file_path = os.path.join(dir_with_diff, f"{module_release_name}.{ext}")
            # If the previous file is not found or doesn't exist, copy the new file as is
            if not previous_file_path or not os.path.exists(previous_file_path):
                shutil.copy(new_file_path, diff_file_path)
                diff_files[ext] = diff_file_path
            else:  # Otherwise, diff the files
                Utils.diff_files(new_file_path, previous_file_path, diff_file_path)
                diff_files[ext] = diff_file_path
        return diff_files

    def _send_notifications_for_module_release(
        self, module_release: ModuleRelease
    ) -> None:
        """
        Send notifications for the module_release installation.

        Args:
            module_release (ModuleRelease): The module_release that was installed.
        """
        MODULE_NAME = self.module
        VERSION = module_release.version

        SUBJECT = f"{MODULE_NAME} {VERSION}"
        TO = "FTP Releases <ftp-release-notifications@gnome.org>"
        SMTP_TO = self._get_smtp_recipients()
        headers = self._get_email_headers(MODULE_NAME, VERSION)

        mail_content, additional_headers = self._compose_mail_content(
            module_release, MODULE_NAME
        )
        headers.update(additional_headers)

        if Utils.is_debug():
            self._log_mail_debug_info(module_release, mail_content, headers)
        else:
            self._send_email(
                SUBJECT, TO, SMTP_TO, mail_content, headers, module_release
            )

    def _get_smtp_recipients(self) -> list[str]:
        """
        Returns a list of email addresses to send the notifications to.

        Returns:
            list[str]: A list of email addresses to send the notifications to.
        """
        return [
            "ftp-release-notifications@gnome.org",
            *[
                f'{maint.get("userid")}@src.gnome.org'
                for maint in self.doap.get_maintainers()
                if maint.get("userid")
            ],
        ]

    def _get_email_headers(self, module_name: str, version: str) -> dict[str, str]:
        """
        Returns a dictionary of email headers.

        Args:
            module_name (str): The name of the module.
            version (str): The version of the module.

        Returns:
            dict[str, str]: A dictionary of email headers.
        """
        return {
            "X-Module-Name": module_name,
            "X-Module-Version": version,
        }

    def _compose_mail_content(
        self,
        module_release: ModuleRelease,
        module_name: str,
    ) -> tuple[str, dict[str, str]]:
        """
        Composes the mail content for the module_release.
        The structure of the mail is as follows:
        - Module description
        - Special files info
        - Download links

        Args:
            module_release (ModuleRelease): The module_release to compose the mail content for.
            module_name (str): The name of the module.

        Returns:
            tuple[str, dict[str, str]]: The mail content and additional headers.
        """
        mail = ""
        headers = {}

        mail += self._add_module_description(module_name)

        special_files_info, special_files_headers = self._get_special_files_info(
            module_release
        )
        mail += special_files_info
        headers.update(special_files_headers)

        download_links, download_links_headers = self._get_download_links(
            module_release
        )
        mail += download_links
        headers.update(download_links_headers)

        return mail, headers

    def _add_module_description(self, module_name: str) -> str:
        """
        Adds the module description retrieved from the DOAP to the mail content.
        If the description is not found, the short description is used.

        Args:
            module_name (str): The name of the module.

        Returns:
            str: The module description formatted for the mail content.
        """
        desc = self.doap.get_description() or self.doap.get_shortdesc()
        if desc:
            return (
                self._print_header(f"About {module_name}")
                + f"{self._format_paragraphs(desc)}\n\n"
            )
        return ""

    def _get_special_files_info(
        self, module_release: ModuleRelease
    ) -> tuple[str, dict[str, str]]:
        """
        Gets special files info for the mail content and headers.

        Args:
            module_release (ModuleRelease): The module_release to get the special files info for.

        Returns:
            tuple[str, dict[str, str]]: A tuple containing the mail content and the headers.
        """
        mail = ""
        headers = {}
        installed_files = self.get_files(module_release.version)
        download_path_base = self._get_download_path_base(module_release.version)

        for ext, _ in module_release.special_files.items():
            relative_path = installed_files.get(ext)
            if not relative_path:
                continue  # Skip if the relative path is not found
            full_path = os.path.join(self.module_dir, relative_path)
            size = os.stat(full_path).st_size
            download_path = f"{download_path_base}/{relative_path}"
            headers[f"X-Module-URL-{ext}"] = download_path
            mail += self._print_header(ext)
            if size < 50000:  # magic const
                with open(full_path, "r") as f:
                    mail += f"{f.read()}\n"
            else:
                mail += f"{download_path}  ({size})\n"
        return mail + "\n", headers

    def _get_download_links(
        self, module_release: ModuleRelease
    ) -> tuple[str, dict[str, str]]:
        """
        Gets download links for the mail content and headers.

        Args:
            module_release (ModuleRelease): The module_release to get the download links for.

        Returns:
            tuple[str, dict[str, str]]: A tuple containing the mail content and the headers.
        """
        mail = self._print_header("Download")
        headers = {}
        installed_files = self.get_files(module_release.version)
        download_path_base = self._get_download_path_base(module_release.version)
        shas = self._get_file_shas(installed_files)

        re_format = re.compile(r".*\.(?P<format>(?:tar\.|diff\.)?[a-z][a-z0-9]*)$")
        for file_path in installed_files.values():
            file_size = os.stat(os.path.join(self.module_dir, file_path)).st_size
            basename = os.path.basename(file_path)
            download_path = f"{download_path_base}/{file_path}"
            file_sha = shas.get(basename)
            format = re_format.match(file_path).group("format")
            mail += f"{download_path}  {file_size}\n"
            if file_sha and format:
                mail += f"  sha256sum: {file_sha}\n"
                headers[f"X-Module-SHA256-{format}"] = file_sha
                headers[f"X-Module-URL-{format}"] = download_path
        return mail + "\n", headers

    def _get_download_path_base(self, version: str) -> str:
        download_page = self.doap.get_download_page()
        return (
            download_page.rstrip("/")
            if download_page
            else f"https://download.gnome.org/sources/{self.module}/{Utils.get_majmin(version)}"
        )

    def _get_file_shas(self, installed_files: dict[str, str]) -> dict[str, str]:
        shas = {}
        sha_path = installed_files.get("sha256sum")
        if sha_path:
            with open(os.path.join(self.module_dir, sha_path), "r") as f:
                for line in f:
                    sha, file = line.strip().split("  ")
                    shas[file] = sha
        return shas

    def _print_header(self, header: str) -> str:
        return f"{header}\n" + ("=" * len(header)) + "\n"

    def _format_paragraphs(self, text: str, width: int = 70) -> str:
        return "\n\n".join(
            textwrap.fill(re.compile(r"\s+").sub(" ", paragraph), width=width)
            for paragraph in text.split("\n\n")
        )

    def _log_mail_debug_info(
        self, module_release: ModuleRelease, mail_content: str, headers: dict[str, str]
    ) -> None:
        logger.debug(
            "%s: Debug is True so won't send notification", module_release.name
        )
        logger.debug("%s: Mail message: %s", module_release.name, mail_content)
        logger.debug(
            "%s: Mail itself would be: %s",
            module_release.name,
            MIMEText(mail_content, _charset="utf-8").as_string(),
        )

    def _send_email(
        self,
        subject: str,
        to: str,
        smtp_to: list[str],
        mail_content: str,
        headers: dict[str, str],
        module_release: ModuleRelease,
    ) -> None:
        msg = MIMEText(mail_content, _charset="utf-8")
        msg["Subject"] = subject
        from_mail = "noreply@gnome.org"
        msg["From"] = formataddr(("GNOME Release Service", from_mail), "utf-8")
        msg["To"] = to
        msg["Date"] = formatdate(localtime=True)
        for key, value in headers.items():
            msg[key] = value

        try:
            with smtplib.SMTP("smtp-int.gnome.org") as server:
                server.sendmail(from_mail, smtp_to, msg.as_string())
                logger.info(
                    "%s: Notifications sent via email to: %s",
                    module_release.name,
                    smtp_to,
                )
        except Exception as e:
            logger.exception("%s: Error sending email: %s", module_release.name, e)
