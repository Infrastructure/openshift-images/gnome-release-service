class GnomeReleaseSystemException(Exception):
    pass


class InternalException(GnomeReleaseSystemException):
    pass


class ExternalException(GnomeReleaseSystemException):
    pass


class FileNotTarballException(ExternalException):
    def __init__(self, file_name: str):
        super().__init__(f"File {file_name} is not a tarball.")


class ModuleVersionAlreadyInstalledException(ExternalException):
    def __init__(self, module_name: str, version: str):
        super().__init__(
            f"Version {version} of module {module_name} is already installed."
        )


class TarballNameParsingException(ExternalException):
    def __init__(self, tarball_name: str):
        super().__init__(f"Could not parse tarball name: {tarball_name}.")


class ModuleReleaseAlreadyBeingInstalledException(ExternalException):
    def __init__(self, module_name: str, version: str):
        super().__init__(
            f"Module release {module_name} : {version} is already being installed by another source."
        )


class GitlabGetException(InternalException):
    def __init__(self, project_name: str, gitlab_server: str):
        super().__init__(
            f"Could not get project {project_name} information from GitLab {gitlab_server}."
        )


class CacheException(InternalException):
    def __init__(self, message: str):
        super().__init__(message)
