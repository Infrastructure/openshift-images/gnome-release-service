from .logger import logger
import os

from .modules.module_info import ModuleInfo
from .modules.module_release import ModuleRelease


class ModulesManager:

    modules_dir = os.path.abspath("sources")
    """
    sources/

    This is a huge tree of released tarballs for each module. If this
    directory does not exist on your mirror, they have chosen to provide
    just the major release tarballs. You will find them under the following
    directories.

    ref: https://ftp-stud.hs-esslingen.de/pub/Mirrors/ftp.gnome.org/README

    Format:

    root\n
    └── module\n
        ├── cache.json\n
        └── maj/majmin version\n
            ├── specific version tarball\n
            ├── specific version sha256sum\n
            ├── specific version news\n
            └── LATEST-IS

    Example:

    root\n
    ├── gnome-calculator\n
    │   ├── 46\n
    │   ├── 45\n
    │   ├── 44\n
    │   ...\n
    │   ├── 3.37\n
    │   │   ├── gnome-calculator-3.37.92.news\n
    │   │   ├── gnome-calculator-3.37.92.sha256sum\n
    │   │   ├── gnome-calculator-3.37.92.tar.xz\n
    │   │   ├── gnome-calculator-3.37.90.news\n
    │   │   ├── gnome-calculator-3.37.90.sha256sum\n
    │   │   ├── gnome-calculator-3.37.90.tar.xz\n
    │   │   └── LATEST-IS-3.37.92\n
    │   ...\n
    │   └── cache.json\n
    ...\n

    All important info about the module is stored in its cache.json file.
    """

    def __init__(self):
        os.makedirs(self.modules_dir, exist_ok=True)

    def get_module_info(self, module_name: str) -> ModuleInfo | None:
        """
        Get the ModuleInfo object for the given module name.

        Args:
            module_name (str): The name of the module.

        Returns:
            ModuleInfo | None: The ModuleInfo object for the given module name or None if not found.
        """
        for d in os.listdir(self.modules_dir):
            d_path = os.path.join(self.modules_dir, d)
            if os.path.isdir(d_path) and d == module_name:
                return ModuleInfo(module_name, d_path)
        return None

    def install_module_release(self, module_release: ModuleRelease) -> None:
        """
        Install a module release. Using mutex ensures only one concurrent release per module.

        Args:
            module_release (ModuleRelease): The module release object.
        """
        module_info = self.get_module_info(module_release.module)
        if module_info is None:
            logger.info("No module for given release found. Will set up a new one.")
            module_info = ModuleInfo(
                module_release.module,
                os.path.join(self.modules_dir, module_release.module),
            )
            logger.info("Created a new module info: %s", module_info.module)
        module_info.install_module_release(module_release)

    def setup_new_module_info(self, module_name: str) -> ModuleInfo:
        """
        Setup a new module info.

        Args:
            module_name (str): The name of the module.

        Returns:
            ModuleInfo: The new module info object.
        """
        module_info = ModuleInfo(
            module_name, os.path.join(self.modules_dir, module_name)
        )
        return module_info
