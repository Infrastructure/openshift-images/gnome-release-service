from .logger import logger
import json
import re
import os
import functools

from .utils import Utils
from .exceptions import CacheException


class Cache:
    """
    Cache class to store information about the directory with modules.

    Debian & Ubuntu parse the cache.json files to check when a package needs to be updated.

    cache.json: [

        json version,

        info - A dictionary where each key is a module name and the value is another dictionary where
        each key is a version and value is another dictionary where each key is a file type and
        the value is the file path relative to the module directory,

        versions - A dictionary where each key is a module name and the value is a list of version numbers,

        ignored - A dictionary where each key is a major version number and the value is a list of ignored files

    ]
    """

    JSON_VERSION = 4

    def __init__(self, dir: str) -> None:
        """
        Initialize the ModuleCache object.

        Args:
            dir (str): The path to the directory with modules.

        Raises:
            CacheException: If the directory with modules does not exist.
        """
        self.dir = dir
        # If the module directory does not exist, raise an exception
        if not os.path.exists(self.dir):
            raise CacheException(
                f"The directory with modules {self.dir} does not exist."
            )
        self.cache_path = os.path.join(self.dir, "cache.json")
        self.update_cache()

    def update_cache(self) -> None:
        """
        Update the cache to reflect the current state of the module directory.

        Sets:
            - cache_json (list): [ json version, info, versions, ignored ]
            - info (dict): A dictionary where each key is a module name and
            the value is another dictionary where each key is a version and
            value is another dictionary where each key is a file type and
            the value is the file path relative to the module directory.
            - versions (dict): A dictionary where each key is a module name and
            the value is a list of version numbers.
            - ignored (dict): A dictionary where each key is a major version number
            and the value is a list of ignored files.
        """
        # Initialize dictionaries to store module information, versions, and ignored files
        self.info: dict[str, dict[str, dict[str, str]]] = {}
        self.versions: dict[str, list[str]] = {}
        self.ignored: dict[str, list[str]] = {}

        # Compile a regular expression to match the expected file name format
        re_file = re.compile(
            r"^"  # Start of the string
            r"(?P<module>.*?)"  # Module
            r"[_-]"  # Underscore or dash
            r"(?:(?P<oldversion>([0-9]+[\.])+[a-z0-9]+)-)?"  # Optional group for old version
            r"(?P<version>(?:(?:[0-9]+\.)*|(?:[0-9]+\-)*)(?:alpha|beta|rc)?[0-9\.]*)"  # Version number
            r"\.(?P<format>(?:tar\.|diff\.)?[a-z][a-z0-9]*)$"  # File format
        )

        # Get the list of directories in the module directory
        majmin_dirs = [
            os.path.join(self.dir, item)
            for item in os.listdir(self.dir)
            if os.path.isdir(os.path.join(self.dir, item))
        ]

        # For each directory
        for majmin_dir in majmin_dirs:
            # Get the list of files in the directory
            for item in os.listdir(majmin_dir):
                item_path = os.path.join(majmin_dir, item)
                r = re_file.match(item)
                if (
                    os.path.isdir(item_path)  # unexpected nested directory
                    or not r  # unexpected file name that doesn't match the regex
                    or not item.startswith(
                        os.path.basename(self.dir)
                    )  # unexpected file not related to the directory
                ):
                    self.ignored.setdefault(os.path.basename(majmin_dir), [])
                    self.ignored[os.path.basename(majmin_dir)].append(item)
                    continue
                # file is ok, add it to info
                module = r.group("module")
                version = r.group("version")
                format = r.group("format")
                self.info.setdefault(module, {})
                self.info[module].setdefault(version, {})
                self.info[module][version][format] = os.path.relpath(
                    item_path, self.dir
                )

        # For each module in the info dictionary
        for module in self.info.keys():
            # Add the sorted list of versions to the versions dictionary
            self.versions[module] = sorted(
                self.info[module], key=functools.cmp_to_key(Utils.version_cmp)
            )

        # Save the cache to the cache file
        self.save_cache()

    def save_cache(self) -> None:
        """
        Save the cache to the cache file.
        """
        self.cache_json = [
            Cache.JSON_VERSION,
            self.info,
            self.versions,
            self.ignored,
        ]
        with open(self.cache_path, "w") as f:
            json.dump(self.cache_json, f)

    def get_versions(self, module: str) -> list[str]:
        """
        Get the versions of a module in cache.

        Args:
            module (str): The name of the module.

        Returns:
            list[str]: The list of version numbers for the module.
        """
        return self.versions.get(module, [])

    def get_files(self, module: str, version: str) -> dict[str, str]:
        """
        Get the files for a module version in cache.

        Args:
            module (str): The name of the module.
            version (str): The version of the module.

        Returns:
            dict[str, str]: A dictionary where each key is a file type and the value is the file path relative to the module directory.
        """
        return self.info.get(module, {}).get(version, {})

    def get_ignored(self) -> list[str]:
        """
        Get the list of ignored files in cache.

        Returns:
            list[str]: The list of ignored files.
        """
        return self.ignored
