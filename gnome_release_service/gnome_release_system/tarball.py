import tarfile
import os

from .exceptions import FileNotTarballException


class Tarball:
    """
    A class to handle tarballs.

    A tarball is always opened for reading with transparent compression.
    """

    def __init__(self, tarball_path: str) -> None:
        """
        Initializes the Tarball object.

        Args:
            tarball_path (str): The path to the tarball.

        Raises:
            FileNotTarballException: If the file is not a tarball.
        """
        self.tarball_path = tarball_path
        if not tarfile.is_tarfile(self.tarball_path):
            raise FileNotTarballException(os.path.basename(self.tarball_path))

    def extract(self, destination: str, desired_files: list[str] | None = None) -> None:
        """
        Extract the tarball to the destination.

        Args:
            destination (str): The destination to extract the tarball to.
            desired_files (list[str] | None): A list of files to extract. If None, all files are extracted.
        """
        with tarfile.open(self.tarball_path, "r") as tar:
            filter = tarfile.data_filter
            if desired_files is not None:
                tar.extractall(destination, members=desired_files, filter=filter)
            else:
                tar.extractall(destination, filter=filter)

    def list_files(self) -> list[str]:
        """
        List the files in the tarball.

        Returns:
            list[str]: A list of files in the tarball.
        """
        with tarfile.open(self.tarball_path, "r") as tar:
            return tar.getnames()

    def get_compression(self) -> str:
        """
        Get the compression method used in the tarball.

        Returns:
            str: The compression method used. If the tarball is not compressed, an empty string is returned.
        """
        extension = os.path.splitext(self.tarball_path)[1][1:]
        return extension if extension != "tar" else ""

    @staticmethod
    def pack_directory(
        directory: str,
        result_path: str,
        compression: str,
        include_parent: bool = False,
    ) -> str:
        """
        Pack a directory into a tarball.

        Args:
            directory (str): The directory to pack.
            result_path (str): The path to save the tarball to.
            compression (str): The compression method to use: "gz", "bz2", "xz", "" (no compression).
            include_parent (bool): Whether to include the parent directory in the tarball.

        Returns:
            str: The path to the tarball.
        """
        with tarfile.open(result_path, f"w:{compression}") as tar:
            if include_parent:
                tar.add(directory, arcname=os.path.basename(directory))
            else:
                for item in os.listdir(directory):
                    tar.add(os.path.join(directory, item), arcname=item)
        return result_path
