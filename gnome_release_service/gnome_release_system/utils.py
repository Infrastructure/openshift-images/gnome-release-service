from .logger import logger
import re
import hashlib
import difflib
import os

from typing import TextIO, Generator


class Utils:
    CHUNKSIZE = 2 * 1024 * 1024  # 2 MB

    @staticmethod
    def is_debug() -> bool:
        """
        Check if the code is running in debug mode.

        Returns:
            bool: True if running in debug mode, False otherwise.
        """
        ENV_VAR_NAME = "DEBUG"
        true_ = ("true", "1")
        false_ = ("false", "0")
        value: str | None = os.getenv(ENV_VAR_NAME, None)
        if value is None or value.lower() not in true_ + false_:
            return False  # value not set or invalid
        if value.lower() in true_:
            return True
        return False  # default to False

    @staticmethod
    def get_majmin(version: str) -> str:
        """
        Returns the folder name to be used.

        This is major.minor for the old versioning scheme and major for the new
        scheme (starting GNOME 40)
        """

        re_majmin = re.compile(r"^([0-9]+\.[0-9]+).*")
        re_maj = re.compile(r"^([0-9]+).*")
        maj = re_maj.sub(r"\1", version)
        if int(maj) >= 40:
            return maj

        return re_majmin.sub(r"\1", version)

    @staticmethod
    def version_cmp(a: str, b: str) -> int:
        """
        Compares two versions

        Logic from Bugzilla::Install::Util::vers_cmp

        Args:
            a (str): First version
            b (str): Second version

        Returns:
            int: -1 if a < b, 0 if a == b, 1 if a > b
        """

        def cmp(x: int, y: int) -> int:
            """
            Compare two numbers.

            Returns:
            -1 if x < y
            0  if x == y
            1  if x > y
            """
            return (x > y) - (x < y)

        re_version = re.compile(r"([-.]|\d+|[^-.\d]+)")

        A = re_version.findall(a.lstrip("0"))
        B = re_version.findall(b.lstrip("0"))

        while A and B:
            a = A.pop(0)
            b = B.pop(0)

            if a == b:
                continue
            elif a == "-":
                return -1
            elif b == "-":
                return 1
            elif a == ".":
                return -1
            elif b == ".":
                return 1
            elif a.isdigit() and b.isdigit():
                c = (
                    cmp(a, b)
                    if (a.startswith("0") or b.startswith("0"))
                    else cmp(int(a, 10), int(b, 10))
                )
                if c:
                    return c
            elif a.isalpha() and b.isdigit():
                if a == "alpha" or a == "beta" or a == "rc":
                    return -1
            elif a.isdigit() and b.isalpha():
                if b == "alpha" or b == "beta" or b == "rc":
                    return 1
            else:
                c = cmp(a.upper(), b.upper())
                if c:
                    return c

        return cmp(len(A), len(B))

    @staticmethod
    def calculate_sha256(file_path: str) -> str:
        """
        Calculate the SHA256 hash of a file.

        Args:
            file_path (str): The path to the file.

        Returns:
            str: The SHA256 hash of the file as a hexadecimal string.
        """
        sha256_hash = hashlib.sha256()
        with open(file_path, "rb") as f:
            # Read the file in chunks to avoid loading the entire file into memory
            for chunk in Utils.read_file_in_chunks(f):
                sha256_hash.update(chunk)
        return sha256_hash.hexdigest()

    @staticmethod
    def diff_files(
        new_file_path: str, old_file_path: str | None, diff_file_path: str
    ) -> None:
        """
        Create a diff between two files and save it to a file.
        If the old file is None, the new file is copied to the diff file path.

        Args:
            new_file (str): The path to the new file.
            old_file (str | None): The path to the old file or None.
            diff_file_path (str): The path to save the diff file.
        """
        # Ignore errors so we can still open non-strictly utf-8 formats such as MacRoman 
        with open(diff_file_path, "w", encoding="UTF-8", errors="replace") as dfp, open(new_file_path, "r", encoding="UTF-8", errors="replace") as nf:
            if old_file_path:
                # Create a diff between the old and new files
                with open(old_file_path, "r", encoding="UTF-8", errors="replace") as of:
                    a = of.readlines()
                    b = nf.readlines()
                    for tag, _, _, j1, j2 in difflib.SequenceMatcher(
                        None, a, b
                    ).get_opcodes():
                        if tag in ["replace", "insert"]:
                            dfp.writelines(b[j1:j2])
            else:
                # Copy the new file to the diff file path
                for chunk in Utils.read_file_in_chunks(nf):
                    dfp.write(chunk)

    @staticmethod
    def read_file_in_chunks(file: TextIO) -> Generator[str, None, None]:
        """Generator to read a file in chunks."""
        while True:
            data = file.read(Utils.CHUNKSIZE)
            if not data:
                break
            yield data
